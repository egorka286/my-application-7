package com.example.myapplication7.data.repository;

import androidx.annotation.NonNull;

import com.example.myapplication7.data.api.FormService;
import com.example.myapplication7.data.models.FieldData;
import com.example.myapplication7.data.models.FormData;
import com.example.myapplication7.data.models.Result;
import com.example.myapplication7.data.models.SentData;
import com.example.myapplication7.domain.models.Field;
import com.example.myapplication7.domain.models.Form;
import com.example.myapplication7.domain.models.Value;
import com.example.myapplication7.domain.repository.FormRepository;
import com.example.myapplication7.domain.usecase.OnGetFormListener;
import com.example.myapplication7.domain.usecase.OnGetResultListener;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormRepositoryImpl implements FormRepository {

    @Inject
    public FormService service;

    public FormRepositoryImpl(FormService service) {
        this.service = service;
    }

    @Override
    public void getForm(OnGetFormListener listener) {
        service.getForm().enqueue(new Callback<FormData>() {
            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                if (response.body() != null) {
                    listener.onGetForm(formDataToForm((FormData) response.body()));
                }
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
            }
        });
    }

    @Override
    public void sendData(@NonNull Form form, OnGetResultListener listener) {
        SentData sentData = new SentData();

        for (Field field : form.fields) {
            sentData.form.put(field.name, field.value);
        }

        service.sendForm(sentData).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(@NonNull Call<Result> call, @NonNull Response<Result> response) {
                if (response.body() != null) {
                    listener.onGetResult(response.body().result);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Result> call, @NonNull Throwable t) {
            }
        });
    }

    @NonNull
    private Form formDataToForm(@NonNull FormData formData) {
        Form form = new Form();
        form.title = formData.title;
        form.image = formData.image;
        form.fields = new ArrayList<>();
        for (FieldData fieldData : formData.fields) {
            form.fields.add(fieldDataToField(fieldData));
        }
        return form;
    }

    @NonNull
    private Field fieldDataToField(@NonNull FieldData fieldData) {
        Field field = new Field();
        field.title = fieldData.title;
        field.name = fieldData.name;
        field.type = fieldData.type;

        if (fieldData.values != null) {
            field.values = new ArrayList<>();
            for (Map.Entry<String, String> item : fieldData.values.entrySet()) {
                field.values.add(new Value(item.getKey(), item.getValue()));
            }
        }
        return field;
    }
}
