package com.example.myapplication7.domain.usecase;

public interface OnGetResultListener {
    void onGetResult(String result);
}
