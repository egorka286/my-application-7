package com.example.myapplication7.domain.models;

public class Value {
    public String key;
    public String value;

    public Value(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
