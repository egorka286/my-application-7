package com.example.myapplication7.domain.usecase;

import com.example.myapplication7.domain.models.Form;
import com.example.myapplication7.domain.repository.FormRepository;

public class SendFormUserCase {
    private final FormRepository repository;

    public SendFormUserCase(FormRepository repository) {
        this.repository = repository;
    }

    public void execute(Form form, OnGetResultListener listener) {
        repository.sendData(form, listener);
    }
}
