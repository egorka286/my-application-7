package com.example.myapplication7.presentation.di;

import com.example.myapplication7.presentation.MainViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(MainViewModel mainViewModel);
}
