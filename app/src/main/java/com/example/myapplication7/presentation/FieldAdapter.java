package com.example.myapplication7.presentation;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication7.R;
import com.example.myapplication7.domain.models.Field;
import com.example.myapplication7.domain.models.Value;

import java.util.List;

public class FieldAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private enum FieldType {TEXT, NUMERIC, LIST, UNKNOWN}

    private final List<Field> fields;

    public FieldAdapter(List<Field> fields) {
        this.fields = fields;
    }

    @Override
    public int getItemViewType(int position) {
        try {
            return FieldType.valueOf(fields.get(position).type).ordinal();
        } catch (Exception ex) {
            return FieldType.UNKNOWN.ordinal();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // TEXT
        if (viewType == FieldType.TEXT.ordinal()) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.text_field, parent, false);
            return new TextViewHolder(view);
            // NUMERIC
        } else if (viewType == FieldType.NUMERIC.ordinal()) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.numeric_field, parent, false);
            return new NumericViewHolder(view);
            // LIST
        } else if (viewType == FieldType.LIST.ordinal()) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_field, parent, false);
            return new ListViewHolder(view, parent.getContext());
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.unknown_field, parent, false);
            return new UnknownViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Field field = fields.get(position);
        // TEXT
        if (holder instanceof TextViewHolder) {
            TextViewHolder textHolder = (TextViewHolder) holder;
            textHolder.title.setText(field.title);
            textHolder.value.setText(field.value);
            textHolder.value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    fields.get(holder.getAdapterPosition()).value = charSequence.toString();
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            // NUMERIC
        } else if (holder instanceof NumericViewHolder) {
            NumericViewHolder numericHolder = (NumericViewHolder) holder;
            numericHolder.title.setText(field.title);
            numericHolder.value.setText(field.value);
            numericHolder.value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    fields.get(holder.getAdapterPosition()).value = charSequence.toString();
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            // LIST
        } else if (holder instanceof ListViewHolder) {
            ListViewHolder listHolder = (ListViewHolder) holder;
            listHolder.title.setText(field.title);

            ArrayAdapter<Value> adapter = new ArrayAdapter<>(listHolder.context, android.R.layout.simple_spinner_item, field.values);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            listHolder.spinner.setAdapter(adapter);
            listHolder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    Adapter adapter = adapterView.getAdapter();
                    fields.get(holder.getAdapterPosition()).value = ((Value) adapter.getItem(i)).key;
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
            for (int index = 0, count = adapter.getCount(); index < count; ++index) {
                if (adapter.getItem(index).key.equals(field.value)) {
                    listHolder.spinner.setSelection(index);
                    break;
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return fields.size();
    }

    public static class TextViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public EditText value;

        public TextViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.text_title);
            value = view.findViewById(R.id.text_value);
        }
    }

    public static class NumericViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public EditText value;

        public NumericViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.numeric_title);
            value = view.findViewById(R.id.numeric_value);
        }
    }

    public static class ListViewHolder extends RecyclerView.ViewHolder {
        public Context context;
        public TextView title;
        public Spinner spinner;

        public ListViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            title = view.findViewById(R.id.list_title);
            spinner = view.findViewById(R.id.spinner);
        }
    }

    public static class UnknownViewHolder extends RecyclerView.ViewHolder {
        public UnknownViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
