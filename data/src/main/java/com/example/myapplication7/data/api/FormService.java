package com.example.myapplication7.data.api;

import com.example.myapplication7.data.models.FormData;
import com.example.myapplication7.data.models.Result;
import com.example.myapplication7.data.models.SentData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface FormService {
    @GET("tt/meta/")
    Call<FormData> getForm();

    @POST("tt/data/")
    Call<Result> sendForm(@Body SentData sentData);
}
