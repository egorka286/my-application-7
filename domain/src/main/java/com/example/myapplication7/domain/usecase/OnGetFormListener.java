package com.example.myapplication7.domain.usecase;

import com.example.myapplication7.domain.models.Form;

public interface OnGetFormListener {
    void onGetForm(Form form);
}
