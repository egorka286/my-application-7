package com.example.myapplication7.presentation;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class ResultDialog extends DialogFragment {
    private String message;

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        return builder.setMessage(message)
                .setPositiveButton("ОК", null)
                .create();
    }

    public ResultDialog message(String message) {
        this.message = message;
        return this;
    }
}
