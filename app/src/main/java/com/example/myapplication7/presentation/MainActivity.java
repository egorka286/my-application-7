package com.example.myapplication7.presentation;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.myapplication7.R;
import com.example.myapplication7.domain.models.Form;
import com.example.myapplication7.domain.usecase.OnGetResultListener;

public class MainActivity extends AppCompatActivity implements OnGetResultListener {

    private ImageView imageView;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    public MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);
        recyclerView = findViewById(R.id.form);
        progressBar = findViewById(R.id.progressBar);

        viewModel = new ViewModelProvider(this).get(MainViewModel.class);

        progressBar.setVisibility(ProgressBar.VISIBLE);

        viewModel.getForm().observe(this, this::showForm);
    }

    public void onSendClick(View view) {
        progressBar.setVisibility(ProgressBar.VISIBLE);
        viewModel.sendForm(this);
    }

    @Override
    public void onGetResult(String result) {
        progressBar.setVisibility(ProgressBar.INVISIBLE);
        new ResultDialog().message(result).show(getSupportFragmentManager(), "resultDialog");
    }

    private void showForm(@NonNull Form form) {
        progressBar.setVisibility(ProgressBar.INVISIBLE);
        setTitle(form.title);
        Glide.with(this).load(form.image).into(imageView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new FieldAdapter(form.fields));
    }
}
