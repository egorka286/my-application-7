package com.example.myapplication7.domain.usecase;

import com.example.myapplication7.domain.repository.FormRepository;

public class GetFormUseCase {
    private final FormRepository repository;

    public GetFormUseCase(FormRepository repository) {
        this.repository = repository;
    }

    public void execute(OnGetFormListener listener) {
        repository.getForm(listener);
    }
}
