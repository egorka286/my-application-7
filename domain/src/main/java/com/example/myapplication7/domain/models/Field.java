package com.example.myapplication7.domain.models;

import java.util.List;

public class Field {
    public String title;
    public String name;
    public String type;
    public List<Value> values;
    public String value;
}
