package com.example.myapplication7.domain.repository;

import com.example.myapplication7.domain.models.Form;
import com.example.myapplication7.domain.usecase.OnGetFormListener;
import com.example.myapplication7.domain.usecase.OnGetResultListener;

public interface FormRepository {
    void getForm(OnGetFormListener listener);

    void sendData(Form form, OnGetResultListener listener);
}
