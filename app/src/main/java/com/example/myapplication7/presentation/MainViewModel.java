package com.example.myapplication7.presentation;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.myapplication7.domain.models.Form;
import com.example.myapplication7.domain.usecase.GetFormUseCase;
import com.example.myapplication7.domain.usecase.OnGetFormListener;
import com.example.myapplication7.domain.usecase.OnGetResultListener;
import com.example.myapplication7.domain.usecase.SendFormUserCase;

import javax.inject.Inject;

public class MainViewModel extends AndroidViewModel implements OnGetFormListener, OnGetResultListener {
    private final MutableLiveData<Form> form = new MutableLiveData<>();
    private OnGetResultListener listener;
    @Inject
    public SendFormUserCase sendFormUserCase;
    @Inject
    public GetFormUseCase getFormUseCase;

    public MainViewModel(@NonNull Application application) {
        super(application);
        ((MyApplication) application).getAppComponent().inject(MainViewModel.this);
        getFormUseCase.execute(this);
    }

    public LiveData<Form> getForm() {
        return form;
    }

    public void sendForm(OnGetResultListener listener) {
        this.listener = listener;
        sendFormUserCase.execute(form.getValue(), this);
    }

    @Override
    public void onGetForm(Form form) {
        this.form.setValue(form);
    }

    @Override
    public void onGetResult(String result) {
        listener.onGetResult(result);
    }
}
