package com.example.myapplication7.domain.models;

import java.util.List;

public class Form {
    public String title;
    public String image;
    public List<Field> fields;
}
