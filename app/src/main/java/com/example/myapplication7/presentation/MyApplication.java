package com.example.myapplication7.presentation;

import android.app.Application;

import com.example.myapplication7.presentation.di.AppComponent;
import com.example.myapplication7.presentation.di.AppModule;
import com.example.myapplication7.presentation.di.DaggerAppComponent;

public class MyApplication extends Application {
    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
