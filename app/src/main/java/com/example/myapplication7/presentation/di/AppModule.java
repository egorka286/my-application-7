package com.example.myapplication7.presentation.di;

import androidx.annotation.NonNull;

import com.example.myapplication7.data.api.FormService;
import com.example.myapplication7.data.repository.FormRepositoryImpl;
import com.example.myapplication7.domain.repository.FormRepository;
import com.example.myapplication7.domain.usecase.GetFormUseCase;
import com.example.myapplication7.domain.usecase.SendFormUserCase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {
    private static final String baseUrl = "http://test.clevertec.ru/";

    @Singleton
    @Provides
    public Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Singleton
    @Provides
    public FormService getFormService(@NonNull Retrofit retrofit) {
        return retrofit.create(FormService.class);
    }

    @Singleton
    @Provides
    public FormRepository getFormRepository() {
        return new FormRepositoryImpl(getRetrofit().create(FormService.class));
    }

    @Singleton
    @Provides
    public GetFormUseCase getGetFormUseCase() {
        return new GetFormUseCase(new FormRepositoryImpl(getRetrofit().create(FormService.class)));
    }

    @Singleton
    @Provides
    public SendFormUserCase getSendFormUserCase() {
        return new SendFormUserCase(new FormRepositoryImpl(getRetrofit().create(FormService.class)));
    }
}
